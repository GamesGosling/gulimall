package cn.migu.gulimall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;


/**
 * 统一异常处理
 * @ControllerAdvice
 */
@MapperScan("cn.migu.gulimall.product.dao")
@SpringBootApplication
@EnableDiscoveryClient
@EnableCaching
@EnableRedisHttpSession
@EnableFeignClients(basePackages = "cn.migu.gulimall.product.feign")
public class GulimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallProductApplication.class, args);
    }

}
