package cn.migu.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.migu.gulimall.common.utils.PageUtils;
import cn.migu.gulimall.coupon.entity.HomeAdvEntity;

import java.util.Map;

/**
 * 首页轮播广告
 *
 * @author 夏沫止水
 * @email HeJieLin@gulimall.com
 * @date 2020-05-22 19:35:30
 */
public interface HomeAdvService extends IService<HomeAdvEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

