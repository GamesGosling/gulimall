package cn.migu.gulimall.coupon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.migu.gulimall.coupon.entity.HomeSubjectSpuEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 专题商品
 *
 * @author 夏沫止水
 * @email HeJieLin@gulimall.com
 * @date 2020-05-22 19:35:30
 */
@Mapper
public interface HomeSubjectSpuDao extends BaseMapper<HomeSubjectSpuEntity> {

}
